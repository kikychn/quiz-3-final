import * as React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import NavBar from "./components/NavBar";
import Home from "./containers/Home";
import Order from "./containers/Order";
import AddProduct from "./containers/AddProduct";

class App extends React.Component {

  render() {
    return (

      <Router>
        <NavBar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/order' component={Order} />
          <Route path='/product/create' component={AddProduct} />
        </Switch>
      </Router>

    )
  }
}

export default App;