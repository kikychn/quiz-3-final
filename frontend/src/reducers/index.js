import {combineReducers} from "redux";
import productReducer from "./productReducer";

const reducers = combineReducers({
  getProducts: productReducer
});

export default reducers;