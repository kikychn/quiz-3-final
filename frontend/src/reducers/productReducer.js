const initState = {
  products: []
};

export default (state = initState, action = {})=>{
  switch (action.type) {
    case "GET_Products":
      return {
        ...state,
        products: action.data
      };
    default:
      return {
        ...state
      };
  }
}