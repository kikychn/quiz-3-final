export const setProducts = list => (dispatch) => {
  dispatch({
    type: 'GET_Products',
    data: list
  })
};