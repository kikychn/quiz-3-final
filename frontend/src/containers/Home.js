import React, {Component} from 'react';
import {getInfo} from "../api";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {setProducts} from "../actions/setProducts";

class Home extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      products : []
    }
  }

  componentDidMount() {
    getInfo().then(list => this.props.setProducts(list));
  }

  render() {
    console.log(this.state.products);
    return (
      <div>
        Hello
        {this.props.products}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);