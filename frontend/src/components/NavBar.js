import React, {Component} from 'react';
import {Link} from "react-router-dom";

class NavBar extends Component {
  render() {
    return (
      <div>
        <li><Link to={'/'}>商城</Link></li>
        <li><Link to={'/order'}>订单</Link></li>
        <li><Link to={'/product/create'}>添加商品</Link></li>
      </div>
    );
  }
}

export default NavBar;