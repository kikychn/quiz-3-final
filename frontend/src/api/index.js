export const createInfo = post =>
  fetch('/api/products', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(post)
  });

export const getInfo = () =>
  fetch('/api/products')
    .then(response => response.json());

export const deleteInfo = id =>
  fetch(`/api/products/${id}`, {
    method: 'DELETE',
  });
