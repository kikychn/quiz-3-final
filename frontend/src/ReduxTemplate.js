// import {init} from "./actions/init";
import {connect} from "react-redux";
import * as React from "react";
import {bindActionCreators} from "redux";

class App extends React.Component {

  handleClick = () => {
    this.props.init()
  };

  render() {
    const name = this.props.app.name;
    return (
      <div>
        <p>{name}</p>
        <button onClick={this.handleClick}></button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => bindActionCreators({
  init
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);