CREATE TABLE product
(
    `id`       BIGINT AUTO_INCREMENT PRIMARY KEY,
    `name`     VARCHAR(128) NOT NULL,
    `price`    DECIMAL      NOT NULL,
    `unit`     VARCHAR(64)  NOT NULL,
    `image_url` VARCHAR(255) NOT NULL
) CHARACTER SET = utf8