package com.twuc.contract;

import java.math.BigDecimal;

public class ShowProductsResponse {
    private String name;
    private BigDecimal price;
    private String unit;
    private String imageUrl;

    public ShowProductsResponse() {
    }

    public ShowProductsResponse(String name, BigDecimal price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
