package com.twuc.contract;

import java.math.BigDecimal;

public class ShowProductsInOrderResponse {
    private String name;
    private BigDecimal price;
    private Integer quantity;
    private String unit;

    public ShowProductsInOrderResponse() {
    }

    public ShowProductsInOrderResponse(String name, BigDecimal price, Integer quantity, String unit) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }
}
