package com.twuc.contract;

import java.math.BigDecimal;

public class AddOrderRequest {
    private String name;
    private BigDecimal price;
    private String unit;
    private String imageUrl;
    private Integer quantity;

    public AddOrderRequest() {
    }

    public AddOrderRequest(String name, BigDecimal price, String unit, String imageUrl, Integer quantity) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
