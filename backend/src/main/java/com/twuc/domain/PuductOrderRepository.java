package com.twuc.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PuductOrderRepository extends JpaRepository<ProductOrder, Long> {
}
