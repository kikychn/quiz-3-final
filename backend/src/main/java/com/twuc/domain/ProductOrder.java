package com.twuc.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class ProductOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public ProductOrder() {
    }

    public Long getId() {
        return id;
    }

}
