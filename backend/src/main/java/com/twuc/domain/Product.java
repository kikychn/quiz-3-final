package com.twuc.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 128)
    private String name;
    @Column(nullable =  false)
    private BigDecimal price;
    @Column(nullable = false, length = 64)
    private String unit;
    @Column(nullable = false)
    private String imageUrl;
    @Column
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private ProductOrder productOrder;

    public Product() {
    }

    public Product(String name, BigDecimal price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Product(String name, BigDecimal price, String unit, String imageUrl, Integer quantity) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Long getId() {
        return id;
    }

    public ProductOrder getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(ProductOrder productOrder) {
        this.productOrder = productOrder;
    }

    public Integer getQuantity() {
        return quantity;
    }

}
