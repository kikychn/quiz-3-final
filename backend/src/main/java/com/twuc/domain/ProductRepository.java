package com.twuc.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByProductOrderId(Long id);

    Product findAllByProductOrderIdAndId(Long orderId, Long productId);
}
