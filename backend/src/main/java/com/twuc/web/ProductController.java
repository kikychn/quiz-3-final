package com.twuc.web;

import com.twuc.contract.AddProductRequest;
import com.twuc.contract.ShowProductsInOrderResponse;
import com.twuc.contract.ShowProductsResponse;
import com.twuc.domain.PuductOrderRepository;
import com.twuc.domain.Product;
import com.twuc.domain.ProductOrder;
import com.twuc.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
public class ProductController {

    private final ProductRepository productRepository;

    private final PuductOrderRepository puductOrderRepository;

    public ProductController(ProductRepository productRepository, PuductOrderRepository puductOrderRepository) {
        this.productRepository = productRepository;
        this.puductOrderRepository = puductOrderRepository;
    }

    @PostMapping("/products")
    public ResponseEntity addProduct(@RequestBody AddProductRequest addProductRequest) throws URISyntaxException {
        Product product = productRepository.saveAndFlush(
                new Product(
                        addProductRequest.getName(),
                        addProductRequest.getPrice(),
                        addProductRequest.getUnit(),
                        addProductRequest.getImageUrl(),
                        addProductRequest.getQuantity()
                ));
        return ResponseEntity.created(new URI("/api/products/" + product.getId())).build();
    }

    @GetMapping("/products")
    public ResponseEntity getAllProducts() {
        List<Product> allProducts = productRepository.findAll();
        Stream<ShowProductsResponse> showProductsResponseStream = allProducts.stream().map(product ->
                new ShowProductsResponse(product.getName(), product.getPrice(), product.getUnit(), product.getImageUrl()));
        return ResponseEntity.ok(showProductsResponseStream);
    }

    @DeleteMapping("products/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        Optional<Product> savedProduct = productRepository.findById(id);
        savedProduct.ifPresent(productRepository::delete);
        return ResponseEntity.ok().build();
    }


    @PostMapping("/orders")
    public ResponseEntity createOrder() throws URISyntaxException {
        ProductOrder productOrder = puductOrderRepository.saveAndFlush(new ProductOrder());
        return ResponseEntity.created(new URI("/api/orders/" + productOrder.getId())).build();
    }

    @PutMapping("/orders/{orderId}/products/{productId}")
    public ResponseEntity addOrder(@PathVariable Long orderId, @PathVariable Long productId) throws URISyntaxException {
        Product product = productRepository.findById(productId).orElseThrow(NoSuchElementException::new);
        ProductOrder productOrder = puductOrderRepository.findById(orderId).orElseThrow(NoSuchElementException::new);
        product.setProductOrder(productOrder);
        productRepository.saveAndFlush(product);
        return ResponseEntity.created(new URI("/api/orders/" + orderId)).build();
    }

    @GetMapping("/orders/{orderId}")
    public ResponseEntity getProductsInOneOrder(@PathVariable Long orderId) {
        List<Product> products = productRepository.findAllByProductOrderId(orderId);
        List<ShowProductsInOrderResponse> result = new ArrayList<>();
        products.forEach(product -> {
            result.add(new ShowProductsInOrderResponse(product.getName(),
                    product.getPrice(), product.getQuantity(), product.getUnit()));
        });
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/orders/{orderId}/products/{productId}")
    public ResponseEntity deleteProductInOneOrder(@PathVariable Long orderId, @PathVariable Long productId) {
        Product product = productRepository.findAllByProductOrderIdAndId(orderId, productId);
        product.setProductOrder(null);
        productRepository.saveAndFlush(product);
        return ResponseEntity.ok().build();
    }
}
