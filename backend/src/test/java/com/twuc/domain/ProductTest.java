package com.twuc.domain;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @Test
    void should_create_product_correctly() {
        Product product = new Product("可乐", BigDecimal.ONE, "瓶", "baidu.com");

        assertEquals("可乐", product.getName());
        assertEquals(BigDecimal.ONE, product.getPrice());
        assertEquals("瓶", product.getUnit());
        assertEquals("baidu.com", product.getImageUrl());
    }
}