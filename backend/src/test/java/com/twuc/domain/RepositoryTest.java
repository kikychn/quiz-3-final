package com.twuc.domain;

import com.twuc.JpaBastTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RepositoryTest extends JpaBastTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PuductOrderRepository puductOrderRepository;

    @Test
    void should_save_and_get_an_product_entity() throws Throwable {
        Product product = productRepository.saveAndFlush(new Product("可乐", BigDecimal.ONE, "瓶", "baidu.com"));

        Product savedProduct = productRepository.findById(product.getId()).orElseThrow(NoSuchElementException::new);
        assertEquals(product.getId(), savedProduct.getId());
    }

    @Test
    void should_save_and_get_an_order_entity() {
        Product product = new Product("可乐", BigDecimal.ONE, "瓶", "baidu.com");
        productRepository.save(product);
        Product product1 = new Product("雪碧", BigDecimal.ONE, "瓶", "baidu.com");
        productRepository.save(product1);
        ProductOrder order = new ProductOrder();
        product.setProductOrder(order);
        product1.setProductOrder(order);
        puductOrderRepository.saveAndFlush(order);

        ProductOrder savedProductOrder = puductOrderRepository.findById(order.getId()).orElseThrow(NoSuchElementException::new);
        assertEquals(order.getId(), savedProductOrder.getId());
    }
}