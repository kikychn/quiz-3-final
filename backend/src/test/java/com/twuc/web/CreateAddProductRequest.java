package com.twuc.web;

import com.twuc.contract.AddProductRequest;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CreateAddProductRequest {
    private String name = "可乐";
    private BigDecimal price = BigDecimal.ONE;
    private String unit = "瓶";
    private String imageUrl = "baidu.com";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public AddProductRequest create() {
        return new AddProductRequest(name, price, unit, imageUrl);
    }

    public AddProductRequest createWithName(String name) {
        return new AddProductRequest(name, price, unit, imageUrl);
    }
}
