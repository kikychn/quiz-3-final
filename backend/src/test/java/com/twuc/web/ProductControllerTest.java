package com.twuc.web;

import com.twuc.JpaBastTest;
import com.twuc.contract.AddProductRequest;
import com.twuc.domain.PuductOrderRepository;
import com.twuc.domain.Product;
import com.twuc.domain.ProductOrder;
import com.twuc.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductControllerTest extends JpaBastTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PuductOrderRepository puductOrderRepository;

    @Autowired
    private CreateAddProductRequest createAddProductRequest;

    @Test
    void should_return_add_a_product_when_post_request_api_products() throws Exception {
        AddProductRequest addProductRequest = createAddProductRequest.create();

        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON)
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/products/1"));

        Product savedProduct = productRepository.findById(1L).orElseThrow(NoSuchElementException::new);
        assertEquals("可乐", savedProduct.getName());
    }


    @Test
    void should_return_all_products_when_get_request_api_products() throws Exception {
        AddProductRequest addProductRequest = createAddProductRequest.createWithName("可乐");
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        AddProductRequest addProductRequest1 = createAddProductRequest.createWithName("雪碧");
        mockPost("/api/products", addProductRequest1, MediaType.APPLICATION_JSON);

        mockGet("/api/products")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name").value("可乐"))
                .andExpect(jsonPath("$[1].name").value("雪碧"));
    }

    @Test
    void should_delete_a_product_when_delete_request_api_products() throws Exception {
        AddProductRequest addProductRequest = createAddProductRequest.createWithName("可乐");
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        AddProductRequest addProductRequest1 = createAddProductRequest.createWithName("雪碧");
        mockPost("/api/products", addProductRequest1, MediaType.APPLICATION_JSON);

        mockGet("/api/products")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
        mockDelete("/api/products/1");
        mockGet("/api/products")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

    }

    @Test
    void should_add_an_order_when_put_request_api_orders() throws Exception {
        AddProductRequest addProductRequest = createAddProductRequest.create();
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        puductOrderRepository.save(new ProductOrder());

        Product savedProduct = productRepository.findById(1L).orElseThrow(NoSuchElementException::new);
        assertNull(savedProduct.getProductOrder());
        mockMvc.perform(put("/api/orders/1/products/1"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/orders/1"));
        Product savedProductWithOrder = productRepository.findById(1L).orElseThrow(NoSuchElementException::new);
        assertNotNull(savedProductWithOrder.getProductOrder());
    }

    @Test
    void should_show_all_products_in_an_order() throws Exception {
        addThreeProductsToAnOrder();

        mockGet("/api/orders/1")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    private void addThreeProductsToAnOrder() throws Exception {
        AddProductRequest addProductRequest = createAddProductRequest.create();
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        mockPost("/api/products", addProductRequest, MediaType.APPLICATION_JSON);
        puductOrderRepository.save(new ProductOrder());
        mockMvc.perform(put("/api/orders/1/products/1"));
        mockMvc.perform(put("/api/orders/1/products/2"));
        mockMvc.perform(put("/api/orders/1/products/3"));
    }

    @Test
    void should_delete_a_product_in_an_order() throws Exception {
        addThreeProductsToAnOrder();

        List<Product> allByProductOrderId = productRepository.findAllByProductOrderId(1L);
        assertEquals(3,allByProductOrderId.size());
        mockDelete("/api/orders/1/products/1");
        List<Product> allByProductOrderId1 = productRepository.findAllByProductOrderId(1L);
        assertEquals(2,allByProductOrderId1.size());
    }
}