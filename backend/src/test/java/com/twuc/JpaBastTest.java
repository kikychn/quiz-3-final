package com.twuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.twuc.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class JpaBastTest {
    @Autowired
    protected MockMvc mockMvc;

    private final ObjectMapper mapper;

    public JpaBastTest() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    protected String serialize(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    protected ResultActions mockPost(String url, Object body, MediaType mediaType) throws Exception {
        return mockMvc.perform(post(url)
                .contentType(mediaType)
                .content(serialize(body)));
    }

	protected ResultActions mockGet(String url) throws Exception {
		return mockMvc.perform(get(url));
	}

    protected ResultActions mockDelete(String url) throws Exception {
        return mockMvc.perform(delete(url));
    }

}
